package com.acmebank.accountmanager

import com.acmebank.accountmanager.application.rest.v1.dto.Account
import com.acmebank.accountmanager.application.rest.v1.dto.Transfer
import com.acmebank.accountmanager.domain.model.Status_Completed
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.test.context.TestPropertySource
import java.math.BigDecimal
import java.util.*


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
    properties = ["spring.datasource.url=jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1"]
)
@Tag("integrationTest")
class AccountManagerApplicationTests {

    @LocalServerPort
    protected var port: Int = 0

    @Autowired
    private lateinit var restTemplate: TestRestTemplate

    @Test
    fun `make two transfers and check account details`() {
        val accountOneNumber = "12345678"
        val accountTwoNumber = "88888888"

        // Check account balances. Assumption database starts with 1,000,000 HKD balance each
        makeTransferAndCheckBalance(
            accountOneNumber,
            accountTwoNumber,
            BigDecimal("62.75"),
            BigDecimal("999937.25"),
            BigDecimal("1000062.75")
        )

        makeTransferAndCheckBalance(
            fromAccountNumber = accountTwoNumber,
            toAccountNumber = accountOneNumber,
            amount = BigDecimal("10.15"),
            expectedFromAccountBalance = BigDecimal("1000052.60"),
            expectedToAccountBalance = BigDecimal("999947.40")
        )
    }

    private fun makeTransferAndCheckBalance(
        fromAccountNumber: String,
        toAccountNumber: String,
        amount: BigDecimal,
        expectedFromAccountBalance: BigDecimal,
        expectedToAccountBalance: BigDecimal
    ) {
        val id = UUID.randomUUID()
        val transferUrl = "http://localhost:$port/v1/transfers/$id"

        // Request a transfer
        val request = HttpEntity<Transfer>(
            Transfer(
                id = id,
                amount = amount,
                fromAccount = fromAccountNumber,
                toAccount = toAccountNumber,
                currency = "HKD"
            )
        )
        val location = this.restTemplate.postForLocation("http://localhost:$port/v1/transfers", request)
        assertThat(location.toString()).isEqualTo(transferUrl)

        // wait assuming time is enough to complete transaction
        Thread.sleep(1000)
        // check transfer to be completed
        val transferResult = this.restTemplate.getForObject(transferUrl, Transfer::class.java)
        assertThat(transferResult).isEqualTo(
            Transfer(
                id = id,
                amount = amount,
                fromAccount = fromAccountNumber,
                toAccount = toAccountNumber,
                currency = "HKD",
                status = Status_Completed
            )
        )

        val fromAccount = this.restTemplate.getForObject("http://localhost:$port/v1/accounts/$fromAccountNumber", Account::class.java)
        assertThat(fromAccount.balance).isEqualTo(expectedFromAccountBalance)

        val toAccount = this.restTemplate.getForObject("http://localhost:$port/v1/accounts/$toAccountNumber", Account::class.java)
        assertThat(toAccount.balance).isEqualTo(expectedToAccountBalance)
    }

}
