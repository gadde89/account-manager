package com.acmebank.accountmanager.config

import org.springframework.data.jpa.repository.config.EnableJpaAuditing

@EnableJpaAuditing
class JpaConfig