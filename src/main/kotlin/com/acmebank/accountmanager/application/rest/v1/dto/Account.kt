package com.acmebank.accountmanager.application.rest.v1.dto

import java.math.BigDecimal

class Account(val number: String, val balance: BigDecimal)