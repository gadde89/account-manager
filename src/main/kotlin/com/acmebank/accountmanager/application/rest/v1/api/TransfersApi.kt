package com.acmebank.accountmanager.application.rest.v1.api

import com.acmebank.accountmanager.application.rest.v1.dto.Transfer
import com.acmebank.accountmanager.domain.model.DuplicateTransferException
import com.acmebank.accountmanager.domain.service.TransferService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.util.*
import com.acmebank.accountmanager.domain.model.Transfer as TransferModel


@Api(description = "Transfers API", tags = ["Transfers"])
@RestController("Transfers")
@RequestMapping("v1/transfers")
class TransfersApi(private val transferService: TransferService) {

    @PostMapping(consumes = ["application/json"])
    fun createTransfer(@RequestBody transfer: Transfer) : ResponseEntity<Any> {
        try {
            val transferResponse = transferService.requestTransfer(transfer.toModel())
            val location = ServletUriComponentsBuilder
                    .fromCurrentRequest().path("/{id}")
                    .buildAndExpand(transferResponse.id).toUri()
            return ResponseEntity.created(location).build()
        } catch (e : DuplicateTransferException) {
            return ResponseEntity.badRequest().body("Duplicate transfer requested")
        }
    }

    @GetMapping(path = ["/{id}"], produces = ["application/json"])
    fun getTransfer(@PathVariable("id") id : UUID) : ResponseEntity<Transfer> {
        val transfer = transferService.getTranferDetails(id)

        return if (transfer != null) {
            ResponseEntity.ok(transfer.toDto())
        } else {
            ResponseEntity.notFound().build()
        }
    }

    fun Transfer.toModel() : TransferModel {
        return TransferModel(
                id = this.id,
                fromAccountNumber = this.fromAccount,
                toAccountNumber = this.toAccount,
                amount = this.amount,
                currency = this.currency)
    }

    fun TransferModel.toDto() : Transfer {
        return Transfer(
                id = this.id,
                fromAccount = this.fromAccountNumber,
                toAccount = this.toAccountNumber,
                amount = this.amount,
                currency = this.currency,
                status = this.status)
    }
}