package com.acmebank.accountmanager.application.rest.v1.api

import com.acmebank.accountmanager.application.rest.v1.dto.Account
import com.acmebank.accountmanager.domain.service.AccountService
import com.acmebank.accountmanager.domain.model.Account as AccountModel
import io.swagger.annotations.Api
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@Api(description = "Accounts API", tags = ["Accounts"])
@RestController("Accounts")
@RequestMapping("v1/accounts")
class AccountsApi(private val accountService: AccountService) {

    @GetMapping(path = ["/{number}"], produces = ["application/json"])
    fun getAccount(@PathVariable("number") accountNumber: String) : ResponseEntity<Account> {
        val account = accountService.getAccountDetails(accountNumber)

        return if (account != null) {
            ResponseEntity.ok(account.toDto())
        } else {
            ResponseEntity.notFound().build()
        }
    }

    fun AccountModel.toDto() : Account {
        return Account(this.number, this.balance)
    }
}