package com.acmebank.accountmanager.application.rest.v1.dto

import java.math.BigDecimal
import java.util.*

data class Transfer(val id: UUID,
                    val amount: BigDecimal,
                    val currency: String,
                    val fromAccount: String,
                    val toAccount: String,
                    var status: String? = null)