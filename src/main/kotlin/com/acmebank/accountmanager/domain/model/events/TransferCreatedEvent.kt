package com.acmebank.accountmanager.domain.model.events

import java.math.BigDecimal
import java.util.*

data class TransferCreatedEvent(val id: UUID,
                                val fromAccount: String,
                                val toAccount: String,
                                val amount: BigDecimal,
                                val currency: String) : Event