package com.acmebank.accountmanager.domain.model.events

import java.util.*

data class TransferFailedEvent(val id: UUID, val comment: String? = null): Event

