package com.acmebank.accountmanager.domain.model.events

import java.util.*

class TransferCompletedEvent(val id: UUID) : Event