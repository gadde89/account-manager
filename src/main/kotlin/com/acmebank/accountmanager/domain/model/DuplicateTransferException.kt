package com.acmebank.accountmanager.domain.model

class DuplicateTransferException: Exception()