package com.acmebank.accountmanager.domain.model

import org.hibernate.envers.Audited
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.domain.Auditable
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import javax.persistence.*

@Audited
@Entity
@EntityListeners(AuditingEntityListener::class)
data class Account(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Int,

        @Column(nullable = false)
        val number: String,

        @Column(nullable = false)
        val balance: BigDecimal,

        @Column
        val currency: String,

        @CreatedDate
        @Column(updatable = false, nullable = false)
        var createdAt: LocalDateTime = ZonedDateTime.now(ZoneId.of("UTC")).toLocalDateTime(),

        @LastModifiedDate
        @Column(updatable = false, nullable = false)
        var lastModifiedDate: LocalDateTime = ZonedDateTime.now(ZoneId.of("UTC")).toLocalDateTime(),

        @Version
        private var version: Int = 0
)