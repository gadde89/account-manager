package com.acmebank.accountmanager.domain.model

import org.hibernate.envers.Audited
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

@Audited
@Entity
@EntityListeners(AuditingEntityListener::class)
data class Transfer(
        @Id
        var id: UUID = UUID.randomUUID(),

        @Column(nullable = false)
        val fromAccountNumber: String,

        @Column(nullable = false)
        val toAccountNumber: String,

        @Column(nullable = false)
        val amount: BigDecimal,

        @Column(nullable = false)
        val currency: String,

        val status: String = Status_Created,

        @CreatedDate
        @Column(updatable = false, nullable = false)
        var createdAt: LocalDateTime = ZonedDateTime.now(ZoneId.of("UTC")).toLocalDateTime(),

        @LastModifiedDate
        @Column(updatable = false, nullable = false)
        var lastModifiedDate: LocalDateTime = ZonedDateTime.now(ZoneId.of("UTC")).toLocalDateTime(),

        @Version
        private var version: Int = 0
) {
        var comment: String? = null
}

const val Status_Created = "Created"
const val Status_Completed = "Completed"
const val Status_Failed = "Failed"