package com.acmebank.accountmanager.domain.service

import com.acmebank.accountmanager.domain.model.Account
import com.acmebank.accountmanager.domain.model.events.TransferCompletedEvent
import com.acmebank.accountmanager.domain.model.events.TransferCreatedEvent
import com.acmebank.accountmanager.domain.model.events.TransferFailedEvent
import com.acmebank.accountmanager.infrastructure.eventbus.EventBus
import com.acmebank.accountmanager.infrastructure.eventbus.EventListener
import com.acmebank.accountmanager.infrastructure.repository.AccountRepository
import com.google.common.eventbus.Subscribe
import org.apache.logging.log4j.kotlin.logger
import org.springframework.stereotype.Service
import java.lang.Exception
import java.math.BigDecimal
import javax.transaction.Transactional

@Service
@EventListener
class AccountService(private val accountRepository: AccountRepository,
                     private val eventBus: EventBus){
    val logger = logger()

    fun getAccountDetails(number: String) : Account? {
        return accountRepository.findByNumber(number)
    }

    @Subscribe
    @Transactional
    fun transferMoney(transferCreatedEvent: TransferCreatedEvent) {
        try {
            val fromAccount = accountRepository.findByNumber(transferCreatedEvent.fromAccount)
            val toAccount = accountRepository.findByNumber(transferCreatedEvent.toAccount)

            if (fromAccount == null || toAccount == null) {
                logger.error("Failed to make transfer ${transferCreatedEvent.id} from Account ${fromAccount?.number} to Account ${toAccount?.number}")
                eventBus.send(TransferFailedEvent(id = transferCreatedEvent.id, comment = "Invalid account number(s) provided"))
                return
            }

            if (failInvalidTransfer(transferCreatedEvent, fromAccount)) {
                return
            }

            val fromAccountBalance = fromAccount.balance.minus(transferCreatedEvent.amount)
            val toAccountBalance = toAccount.balance.plus(transferCreatedEvent.amount)

            val updatedFromAccount = fromAccount.copy(balance = fromAccountBalance)
            val updatedToAccount = toAccount.copy(balance = toAccountBalance)

            accountRepository.saveAll(listOf( updatedFromAccount, updatedToAccount))

            eventBus.send(TransferCompletedEvent(id = transferCreatedEvent.id))
            logger.info("Transfer ${transferCreatedEvent.id} processed succesfully")
        } catch (e: Exception) {
            logger.error("Unexpected error occurred while making transfer ${transferCreatedEvent.id}", e)
            eventBus.send(TransferFailedEvent(id = transferCreatedEvent.id, comment = "Transfer failed"))
        }
    }

    private fun failInvalidTransfer(transferCreatedEvent: TransferCreatedEvent, fromAccount: Account) : Boolean {
        if (transferCreatedEvent.currency != "HKD") {
            logger.error("Failed to make transfer ${transferCreatedEvent.id} with currency ${transferCreatedEvent.currency}")
            eventBus.send(TransferFailedEvent(id = transferCreatedEvent.id, comment = "Invalid currency for transfer"))
            return true
        }

        val fromAccountBalance = fromAccount.balance.minus(transferCreatedEvent.amount)
        if (fromAccountBalance.compareTo(BigDecimal.ZERO) < 0) {
            eventBus.send(TransferFailedEvent(id = transferCreatedEvent.id, comment = "Insufficient account balance"))
            return true
        }

        return false
    }

}