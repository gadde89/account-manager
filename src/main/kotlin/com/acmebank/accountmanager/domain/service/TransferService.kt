package com.acmebank.accountmanager.domain.service

import com.acmebank.accountmanager.domain.model.DuplicateTransferException
import com.acmebank.accountmanager.domain.model.Status_Completed
import com.acmebank.accountmanager.domain.model.Status_Failed
import com.acmebank.accountmanager.domain.model.Transfer
import com.acmebank.accountmanager.domain.model.events.TransferCompletedEvent
import com.acmebank.accountmanager.domain.model.events.TransferCreatedEvent
import com.acmebank.accountmanager.domain.model.events.TransferFailedEvent
import com.acmebank.accountmanager.infrastructure.eventbus.EventBus
import com.acmebank.accountmanager.infrastructure.eventbus.EventListener
import com.acmebank.accountmanager.infrastructure.repository.TransferRepository
import com.google.common.eventbus.Subscribe
import org.springframework.stereotype.Service
import java.util.*

@Service
@EventListener
class TransferService(private val transferRepository: TransferRepository,
                      private val eventBus: EventBus) {

    @Throws(DuplicateTransferException::class)
    fun requestTransfer(transfer: Transfer): Transfer {
        val existingTransfer = transferRepository.findById(transfer.id)
        if (existingTransfer.isPresent) {
            throw DuplicateTransferException()
        }

        val savedTransfer = transferRepository.save(transfer)
        eventBus.send(TransferCreatedEvent(savedTransfer.id,
                savedTransfer.fromAccountNumber,
                savedTransfer.toAccountNumber,
                savedTransfer.amount,
                savedTransfer.currency))
        return savedTransfer
    }

    fun getTranferDetails(id: UUID): Transfer? {
        return transferRepository.getOne(id)
    }

    @Subscribe
    fun transferCompleted(transferCompletedEvent: TransferCompletedEvent) {
        val transfer = transferRepository.getOne(transferCompletedEvent.id)
        val completedTransfer = transfer.copy(status = Status_Completed)
        transferRepository.save(completedTransfer)
    }


    @Subscribe
    fun transferFailed(transferFailedEvent: TransferFailedEvent) {
        val transfer = transferRepository.getOne(transferFailedEvent.id)
        val failedTransfer = transfer.copy(status = Status_Failed)
        transferRepository.save(failedTransfer)
    }
}
