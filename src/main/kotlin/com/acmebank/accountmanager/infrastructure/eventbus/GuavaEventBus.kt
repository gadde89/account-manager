package com.acmebank.accountmanager.infrastructure.eventbus

import com.acmebank.accountmanager.domain.model.events.Event
import org.springframework.stereotype.Component

@Component
class GuavaEventBus(private val eventBus : com.google.common.eventbus.EventBus): EventBus {
    override fun send(event: Event) {
        eventBus.post(event)
    }

}