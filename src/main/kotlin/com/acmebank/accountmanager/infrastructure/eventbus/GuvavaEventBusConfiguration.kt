package com.acmebank.accountmanager.infrastructure.eventbus

import com.google.common.eventbus.AsyncEventBus
import com.google.common.eventbus.EventBus
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.Executors

@Configuration
class GuvavaEventBusConfiguration {

    @Bean
    protected fun domainEventBus(@Value("\${guava.domain.event.bus.executors:1}") threads: Int) : EventBus {
        return AsyncEventBus(Executors.newFixedThreadPool(threads))
    }

    @Bean
    fun guavaEvenBustListenersRegisterer(eventBus: EventBus) : GuavaEvenBustListenersRegisterer {
        return GuavaEvenBustListenersRegisterer(eventBus)
    }
}