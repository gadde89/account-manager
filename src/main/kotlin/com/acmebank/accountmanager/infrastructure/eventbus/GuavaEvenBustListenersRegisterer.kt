package com.acmebank.accountmanager.infrastructure.eventbus

import com.google.common.eventbus.EventBus
import org.springframework.beans.BeansException
import org.springframework.beans.factory.config.BeanPostProcessor


class GuavaEvenBustListenersRegisterer(private val eventBus: EventBus): BeanPostProcessor {

    @Throws(BeansException::class)
    override fun postProcessBeforeInitialization(bean: Any, beanName: String): Any? {
        return bean
    }

    @Throws(BeansException::class)
    override fun postProcessAfterInitialization(bean: Any, beanName: String): Any? {
        if (bean.javaClass.isAnnotationPresent(EventListener::class.java)) {
            eventBus.register(bean)
        }
        return bean
    }
}