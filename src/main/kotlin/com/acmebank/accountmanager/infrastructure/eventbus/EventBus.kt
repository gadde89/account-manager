package com.acmebank.accountmanager.infrastructure.eventbus

import com.acmebank.accountmanager.domain.model.events.Event

interface EventBus {
    fun send(event: Event)
}