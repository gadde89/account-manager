package com.acmebank.accountmanager.infrastructure.eventbus

import com.google.common.eventbus.Subscribe
import java.lang.annotation.Inherited
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@Inherited
annotation class EventListener