package com.acmebank.accountmanager.infrastructure.repository

import com.acmebank.accountmanager.domain.model.Account
import org.springframework.data.jpa.repository.JpaRepository

interface AccountRepository : JpaRepository<Account, Int> {
    fun findByNumber(number : String) : Account?
}