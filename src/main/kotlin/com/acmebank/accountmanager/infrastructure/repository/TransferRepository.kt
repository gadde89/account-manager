package com.acmebank.accountmanager.infrastructure.repository

import com.acmebank.accountmanager.domain.model.Transfer
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface TransferRepository : JpaRepository<Transfer, UUID> {
}