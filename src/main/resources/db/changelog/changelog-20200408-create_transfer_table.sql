--liquibase formatted sql
--changeset shyam:20200408-create_transfer_table

create table transfer (
    id UUID PRIMARY KEY,
    from_account_number VARCHAR(30) NOT NULL,
    to_account_number VARCHAR(30) NOT NULL,
    amount DECIMAL(20,2) NOT NULL,
    currency VARCHAR(10) NOT NULL,
    status VARCHAR(30) NOT NULL,
    comment VARCHAR(512),
    created_at TIMESTAMP,
    last_modified_date TIMESTAMP,
    version INTEGER
);

create table transfer_aud (
    id UUID,
    from_account_number VARCHAR(30) NOT NULL,
    to_account_number VARCHAR(30) NOT NULL,
    amount DECIMAL(20,2) NOT NULL,
    currency VARCHAR(10) NOT NULL,
    status VARCHAR(30) NOT NULL,
    comment varchar(512),
    created_at TIMESTAMP,
    last_modified_date TIMESTAMP,
    rev INTEGER,
    revtype TINYINT
 );