--liquibase formatted sql
--changeset shyam:20200407-add_accounts

insert into account (number, balance, currency, created_at, last_modified_date, version) values ('12345678', '1000000.00', 'HKD', now(), now(), 0);
insert into account (number, balance, currency, created_at, last_modified_date, version) values ('88888888', '1000000.00', 'HKD', now(), now(), 0);