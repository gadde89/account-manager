--liquibase formatted sql
--changeset shyam:20200404-create_account_table

create table account (
    id IDENTITY PRIMARY KEY,
    number VARCHAR(30) NOT NULL,
    balance DECIMAL(20, 2) NOT NULL,
    currency VARCHAR(12) NOT NULL,
    created_at TIMESTAMP,
    last_modified_date TIMESTAMP,
    version INTEGER
);

create table account_aud (
    id BIGINT,
    number VARCHAR(30) NOT NULL,
    balance DECIMAL(20, 2) NOT NULL,
    currency VARCHAR(12) NOT NULL,
    created_at TIMESTAMP,
    last_modified_date TIMESTAMP,
    rev INTEGER,
    revtype TINYINT
);