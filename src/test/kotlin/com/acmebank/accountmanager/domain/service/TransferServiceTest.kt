package com.acmebank.accountmanager.domain.service

import com.acmebank.accountmanager.domain.model.*
import com.acmebank.accountmanager.domain.model.events.TransferCompletedEvent
import com.acmebank.accountmanager.domain.model.events.TransferCreatedEvent
import com.acmebank.accountmanager.domain.model.events.TransferFailedEvent
import com.acmebank.accountmanager.infrastructure.eventbus.EventBus
import com.acmebank.accountmanager.infrastructure.repository.TransferRepository
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.inOrder
import org.mockito.Mockito.never
import org.mockito.junit.jupiter.MockitoExtension
import java.math.BigDecimal
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class TransferServiceTest {

    @InjectMocks
    private lateinit var transferService: TransferService

    @Mock
    private lateinit var transferRepository: TransferRepository

    @Mock
    private lateinit var eventBus: EventBus

    private val transferCreatedEventCaptor = argumentCaptor<TransferCreatedEvent>()

    private val transferCaptor = argumentCaptor<Transfer>()

    @Test
    fun `requestTransfer raises transfer created event and returns saved transfer`() {
        val id = UUID.randomUUID()
        val transfer = createMockTransfer(id)

        val savedTransfer = createMockTransfer(id)
        whenever(transferRepository.findById(transfer.id)).thenReturn(Optional.empty())
        whenever(transferRepository.save(transfer)).thenReturn(savedTransfer)

        val result = transferService.requestTransfer(transfer)

        val inOrder = inOrder(transferRepository, eventBus)
        inOrder.verify(transferRepository).save(transfer)
        inOrder.verify(eventBus).send(transferCreatedEventCaptor.capture())

        assertEquals(savedTransfer.id, transferCreatedEventCaptor.firstValue.id)
        assertSame(savedTransfer, result)
    }

    @Test
    fun `requestTransfer throws duplicate transfer exception when duplicate ID is requested`() {
        val id = UUID.randomUUID()
        val transfer = createMockTransfer(id)

        val existingTransfer = createMockTransfer(id)
        whenever(transferRepository.findById(transfer.id)).thenReturn(Optional.of(existingTransfer))

        assertThrows(DuplicateTransferException::class.java, {transferService.requestTransfer(transfer)})
    }

    @Test
    fun `getTranferDetails returns transfer details given valid id`() {
        val id = UUID.randomUUID()

        val savedTransfer = createMockTransfer(id)
        whenever(transferRepository.getOne(id)).thenReturn(savedTransfer)

        val result = transferService.getTranferDetails(id)

        assertSame(savedTransfer, result)
    }

    @Test
    fun `getTranferDetails returns null details given id does not exist`() {
        val id = UUID.randomUUID()
        whenever(transferRepository.getOne(id)).thenReturn(null)

        val result = transferService.getTranferDetails(id)

        assertNull(result)
    }

    @Test
    fun `tranferCompleted saves transfer  with status completed`() {
        val id = UUID.randomUUID()
        val transferCompletedEvent = TransferCompletedEvent(id)
        val savedTransfer = createMockTransfer(id)
        whenever(transferRepository.getOne(id)).thenReturn(savedTransfer)


        transferService.transferCompleted(transferCompletedEvent)

        verify(transferRepository).save(transferCaptor.capture())
        val updatedTransfer = transferCaptor.firstValue
        assertEquals(Status_Completed, updatedTransfer.status)

        assertEquals(savedTransfer.id, updatedTransfer.id)
        assertEquals(savedTransfer.fromAccountNumber, updatedTransfer.fromAccountNumber)
        assertEquals(savedTransfer.toAccountNumber, updatedTransfer.toAccountNumber)
        assertEquals(savedTransfer.amount, updatedTransfer.amount)
        assertEquals(savedTransfer.currency, updatedTransfer.currency)
    }

    @Test
    fun `transferFailed saves transfer with status failed`() {
        val id = UUID.randomUUID()
        val transferFailedEvent = TransferFailedEvent(id)
        val savedTransfer = createMockTransfer(id)
        whenever(transferRepository.getOne(id)).thenReturn(savedTransfer)

        transferService.transferFailed(transferFailedEvent)

        verify(transferRepository).save(transferCaptor.capture())
        val updatedTransfer = transferCaptor.firstValue
        assertEquals(Status_Failed, updatedTransfer.status)

        assertEquals(savedTransfer.id, updatedTransfer.id)
        assertEquals(savedTransfer.fromAccountNumber, updatedTransfer.fromAccountNumber)
        assertEquals(savedTransfer.toAccountNumber, updatedTransfer.toAccountNumber)
        assertEquals(savedTransfer.amount, updatedTransfer.amount)
        assertEquals(savedTransfer.currency, updatedTransfer.currency)
    }

    fun createMockTransfer(id: UUID, status: String = Status_Created) : Transfer {
        return Transfer(id = id,
                fromAccountNumber = "12345678",
                toAccountNumber = "88888888",
                amount = BigDecimal("200.00"),
                currency = "HKD",
                status = status)
    }

}