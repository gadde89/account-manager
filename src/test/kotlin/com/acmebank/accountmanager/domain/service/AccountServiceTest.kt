package com.acmebank.accountmanager.domain.service

import com.acmebank.accountmanager.domain.model.Account
import com.acmebank.accountmanager.domain.model.events.TransferCompletedEvent
import com.acmebank.accountmanager.domain.model.events.TransferCreatedEvent
import com.acmebank.accountmanager.domain.model.events.TransferFailedEvent
import com.acmebank.accountmanager.infrastructure.eventbus.EventBus
import com.acmebank.accountmanager.infrastructure.repository.AccountRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import java.lang.RuntimeException
import java.math.BigDecimal
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class AccountServiceTest {

    @InjectMocks
    private lateinit var accountService: AccountService

    @Mock
    private lateinit var accountRepository: AccountRepository

    @Mock
    private lateinit var eventBus: EventBus

    private val accountsCaptor = argumentCaptor<Iterable<Account>>()

    private val transferCompletedEventCaptor = argumentCaptor<TransferCompletedEvent>()
    private val transferFailedEventCaptor = argumentCaptor<TransferFailedEvent>()

    @Test
    fun `getAccountDetails returns account given account number exists`() {
        val accountNumber = "12345678"
        val account = Account(id = 123, number = accountNumber, balance = BigDecimal("1000000.00"), currency = "HKD")
        whenever(accountRepository.findByNumber(accountNumber)).thenReturn(account)

        val result = accountService.getAccountDetails(accountNumber)

        assertNotNull(account)
        assertSame(account, result)
    }

    @Test
    fun `getAccountDetails returns null given account number does not exist`() {
        val accountNumber = "23466904"

        val result = accountService.getAccountDetails(accountNumber)

        assertNull(result)
    }

    @Test
    fun `transferMoney moves money from one account to another`() {
        val transferId = UUID.randomUUID()
        val transferCreatedEvent = mockTransferCreateEvent(transferId)
        whenever(accountRepository.findByNumber("12345678")).thenReturn(mockAccount(accountNumber = "12345678"))
        whenever(accountRepository.findByNumber("88888888")).thenReturn(mockAccount(accountNumber = "88888888"))

        accountService.transferMoney(transferCreatedEvent)

        verify(accountRepository).saveAll(accountsCaptor.capture())
        verify(eventBus).send(transferCompletedEventCaptor.capture())

        val accounts = accountsCaptor.firstValue.iterator()
        val fromAccount = accounts.next()
        val toAccount = accounts.next()
        assertFalse(accounts.hasNext())

        assertEquals(BigDecimal("999799.75"), fromAccount.balance)
        assertEquals(BigDecimal("1000200.25"), toAccount.balance)

        assertEquals(transferId, transferCompletedEventCaptor.firstValue.id)
    }

    @Test
    fun `transferMoney send transfer failed event when from account is invalid`() {
        val transferId = UUID.randomUUID()
        val transferCreatedEvent = mockTransferCreateEvent(id = transferId, fromAccount = "12345679")
        whenever(accountRepository.findByNumber("12345679")).thenReturn(mockAccount(accountNumber = "12345678"))
        whenever(accountRepository.findByNumber("88888888")).thenReturn(null)

        accountService.transferMoney(transferCreatedEvent)

        verify(eventBus).send(transferFailedEventCaptor.capture())
        assertEquals(transferId, transferFailedEventCaptor.firstValue.id)
        assertEquals("Invalid account number(s) provided", transferFailedEventCaptor.firstValue.comment)
    }

    @Test
    fun `transferMoney send transfer failed event when to account is invalid`() {
        val transferId = UUID.randomUUID()
        val transferCreatedEvent = mockTransferCreateEvent(id = transferId, toAccount = "88888889")
        whenever(accountRepository.findByNumber("12345678")).thenReturn(null)
        whenever(accountRepository.findByNumber("88888889")).thenReturn(mockAccount(accountNumber = "88888889"))

        accountService.transferMoney(transferCreatedEvent)

        verify(eventBus).send(transferFailedEventCaptor.capture())
        assertEquals(transferId, transferFailedEventCaptor.firstValue.id)
        assertEquals("Invalid account number(s) provided", transferFailedEventCaptor.firstValue.comment)
    }

    @Test
    fun `transferMoney send transfer failed event when account balance is insufficient`() {
        val transferId = UUID.randomUUID()
        val transferCreatedEvent = mockTransferCreateEvent(id = transferId, fromAccount = "12345678")
        whenever(accountRepository.findByNumber("12345678")).thenReturn(mockAccount(accountNumber = "12345678", balance = BigDecimal("100.00")))
        whenever(accountRepository.findByNumber("88888888")).thenReturn(mockAccount(accountNumber = "88888888"))

        accountService.transferMoney(transferCreatedEvent)

        verify(eventBus).send(transferFailedEventCaptor.capture())
        assertEquals(transferId, transferFailedEventCaptor.firstValue.id)
        assertEquals("Insufficient account balance", transferFailedEventCaptor.firstValue.comment)
    }

    @Test
    fun `transferMoney send transfer failed event when currency is not HKD`() {
        val transferId = UUID.randomUUID()
        val transferCreatedEvent = mockTransferCreateEvent(id = transferId, fromAccount = "12345678").copy(currency = "USD")
        whenever(accountRepository.findByNumber("12345678")).thenReturn(mockAccount(accountNumber = "12345678"))
        whenever(accountRepository.findByNumber("88888888")).thenReturn(mockAccount(accountNumber = "88888888"))

        accountService.transferMoney(transferCreatedEvent)

        verify(eventBus).send(transferFailedEventCaptor.capture())
        assertEquals(transferId, transferFailedEventCaptor.firstValue.id)
        assertEquals("Invalid currency for transfer", transferFailedEventCaptor.firstValue.comment)
    }


    @Test
    fun `transferMoney send transfer failed event when runtime exception happens`() {
        val transferId = UUID.randomUUID()
        val transferCreatedEvent = mockTransferCreateEvent(id = transferId, fromAccount = "12345678")
        whenever(accountRepository.findByNumber("12345678")).thenReturn(mockAccount(accountNumber = "12345678"))
        whenever(accountRepository.findByNumber("88888888")).thenReturn(mockAccount(accountNumber = "88888888"))
        whenever(accountRepository.saveAll(any<Iterable<Account>>())).thenThrow(RuntimeException())

        accountService.transferMoney(transferCreatedEvent)

        verify(eventBus).send(transferFailedEventCaptor.capture())
        assertEquals(transferId, transferFailedEventCaptor.firstValue.id)
        assertEquals("Transfer failed", transferFailedEventCaptor.firstValue.comment)
    }

    private fun mockTransferCreateEvent(id: UUID, fromAccount: String = "12345678", toAccount: String = "88888888") : TransferCreatedEvent {
        return TransferCreatedEvent(id = id,
                fromAccount = fromAccount,
                toAccount = toAccount,
                amount = BigDecimal("200.25"),
                currency = "HKD"
        )
    }

    private fun mockAccount(accountNumber: String, balance: BigDecimal = BigDecimal("1000000.00")): Account {
        return Account(id = 1234, number = accountNumber, balance = balance, currency = "HKD")
    }

}