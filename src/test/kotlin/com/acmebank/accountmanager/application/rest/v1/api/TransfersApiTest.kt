package com.acmebank.accountmanager.application.rest.v1.api

import com.acmebank.accountmanager.application.rest.v1.dto.Transfer
import com.acmebank.accountmanager.domain.model.DuplicateTransferException
import com.acmebank.accountmanager.domain.service.TransferService
import com.fasterxml.jackson.databind.ObjectMapper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.header
import java.math.BigDecimal
import java.util.*
import com.acmebank.accountmanager.domain.model.Transfer as TransferModel


@WebMvcTest(TransfersApi::class)
internal class TransfersApiTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var mapper: ObjectMapper

    @MockBean
    private lateinit var transferService: TransferService

    @Test
    fun `create transfer successfully given valid details`() {
        val transferId = UUID.randomUUID()
        val transferDto = Transfer(id = transferId, amount = BigDecimal("100.12"), fromAccount = "12345678", toAccount = "88888888", currency = "HKD")

        val transfer = TransferModel(
                id = transferId,
                amount = BigDecimal("100.12"),
                fromAccountNumber = "12345678",
                toAccountNumber = "88888888",
                currency = "HKD")
        whenever(transferService.requestTransfer(any())).thenReturn(transfer)

        mockMvc.perform(MockMvcRequestBuilders.post("/v1/transfers")
                .content(mapper.writeValueAsString(transferDto))
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(header().string("Location", "http://localhost/v1/transfers/" + transferId))
    }

    @Test
    fun `create transfer fails with 400 when a duplicate id is provided`() {
        val transferId = UUID.randomUUID()
        val transferDto = Transfer(id = transferId, amount = BigDecimal("100.12"), fromAccount = "12345678", toAccount = "88888888", currency = "HKD")

        whenever(transferService.requestTransfer(any())).thenThrow(DuplicateTransferException())

        mockMvc.perform(MockMvcRequestBuilders.post("/v1/transfers")
                .content(mapper.writeValueAsString(transferDto))
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(content().string("Duplicate transfer requested"))
    }

    @Test
    fun `create transfer fails with 400 when no account provided`() {
        mockMvc.perform(MockMvcRequestBuilders.post("/v1/transfers")
                .content("""{"amount": 200.00, currency="HKD"}""")
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
    }

    @Test
    fun `create transfer fails with 400 when missing amount`() {
        mockMvc.perform(MockMvcRequestBuilders.post("/v1/transfers")
                .content("""{"fromAccount":"12345679", "toAccount":"88888888", "currency":"HKD"}""")
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
    }

    @Test
    fun `get tranfser returns trasnfer details`() {
        val transfer = TransferModel(
                id = UUID.fromString("864e24ae-ca66-407c-888a-850d7e23f12b"),
                amount = BigDecimal("100.12"),
                fromAccountNumber = "12345678",
                toAccountNumber = "88888888",
                currency = "HKD")
        whenever(transferService.getTranferDetails(UUID.fromString("864e24ae-ca66-407c-888a-850d7e23f12b")))
                .thenReturn(transfer)

        mockMvc.perform(MockMvcRequestBuilders.get("/v1/transfers/864e24ae-ca66-407c-888a-850d7e23f12b")
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("""{"id": "864e24ae-ca66-407c-888a-850d7e23f12b", "amount": 100.12, "fromAccount":"12345678", "toAccount":"88888888", "currency":"HKD"}"""))

    }

    @Test
    fun `get tranfser returns 404 status for invalid transfer id`() {
        whenever(transferService.getTranferDetails(UUID.fromString("124e24ae-ca66-407c-888a-850d7e23f12b")))
                .thenReturn(null)

        mockMvc.perform(MockMvcRequestBuilders.get("/v1/transfers/864e24ae-ca66-407c-888a-850d7e23f12b")
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
    }

}