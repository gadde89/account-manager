package com.acmebank.accountmanager.application.rest.v1.api


import com.acmebank.accountmanager.domain.model.Account
import com.acmebank.accountmanager.domain.service.AccountService
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.math.BigDecimal


@WebMvcTest(AccountsApi::class)
internal class AccountsApiTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    lateinit var accountService: AccountService

    @Test
    fun `get account gives account for valid account number`() {
        val accountNumber = "12345678"
        val account = Account(id = 123, number = accountNumber, balance = BigDecimal("1000000.00"), currency = "HKD")
        `when`(accountService.getAccountDetails(accountNumber)).thenReturn(account)

        mockMvc.perform(get("/v1/accounts/" + accountNumber)
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("""{"number": "12345678", "balance": 1000000.00}"""))
    }

    @Test
    fun `get account returns 404 when account number does not exist`() {
        val accountNumber = "34567891"
        whenever(accountService.getAccountDetails(accountNumber)).thenReturn(null)

        mockMvc.perform(get("/v1/accounts/" + accountNumber)
                .contentType("application/json"))
                .andExpect(status().isNotFound())
    }
}